<?php
/**
 * IDE: IntelliJ IDEA
 * Project: Proje-Kariyer
 * Owner: M. Kalender
 * Contact: muhammedkalender@protonmail.com
 * Date: 16-03-2019
 * Time: 14:12
 */


$arrLang = [
    "auth_problem" => "Bu işlem için yetkin yok",
    "check_null" => "[%PARAM1%] Boş olamaz",
    "check_short" => "[%PARAM1%], [%PARAM2%] Harften kısa olamaz",
    "check_long" => "[%PARAM1%], [%PARAM2%] Harften uzun olamaz",
    "check_type" => "[%PARAM1%], Geçerli bir değer değil",
    "var_username" => "Kullanıcı Adı",
    "var_email" => "Email",
    "var_type" => "Tip",
    "var_name" => "İsim",
    "var_surname" => "Soyisim",
    "var_description" => "Açıklama",
    "var_website" => "Websitesi",
    "var_picture" => "Resim",
    "var_password" => "Şifre",
    "var_password_repeat" => "Şifre Tekrarı",
    "password_repeat" => "Şifreler uyuşmuyor",
    "var_disable" => "Engel",
    "var_smoke" => "Sigara İçme",
    "var_gsm" => "GSM",
    "var_tel" => "Telefon",
    "var_fax" => "Fax",
    "login_user" => "Kullanıcı Adı",
    "login_password" => "Şifre",
    "wrong_login" => "Kullanıcı Adı veya Şifre Yanlış",
    "login_system_error" => "Sunucu tarafında hata oluştu, lütfen biraz sorna tekrar deneyin"

];

//Lang
function l($ParamLang, $ParamValue1 = "", $ParamValue2 = ""){
    global $arrLang;

    if(isset($arrLang[$ParamLang])) {
        $result = $arrLang[$ParamLang];

        if($ParamValue1 != "") {
            $result = str_replace("[%PARAM1%]", $ParamValue1, $result);
        }

        if($ParamValue2 != "") {
            $result = str_replace("[%PARAM2%]", $ParamValue2, $result);
        }

        return $result;
    }

    return $ParamLang . $ParamValue1 . $ParamValue2;
}

//Print
function p($ParamLang){
    global $arrLang;

    if(isset($arrLang[$ParamLang])) {
        echo $arrLang[$ParamLang];
    }

    echo $ParamLang;
}