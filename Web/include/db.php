<?php
/**
 * IDE: IntelliJ IDEA
 * Project: Proje-Kariyer
 * Owner: M. Kalender
 * Contact: muhammedkalender@protonmail.com
 * Date: 16-03-2019
 * Time: 14:11
 */

$host = 'localhost';
$db_name = 'kariyer';
$user = 'root';
$pass = '';
$charset = 'utf8mb4';

$db;

$dsn = "mysql:host=$host;dbname=$db_name;charset=$charset";
$options = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES => false,
];
try {
    $db = new PDO($dsn, $user, $pass, $options);
} catch (PDOException $e) {
    throw new PDOException($e->getMessage(), (int)$e->getCode());
}
