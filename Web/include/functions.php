<?php
/**
 * IDE: IntelliJ IDEA
 * Project: Proje-Kariyer
 * Owner: M. Kalender
 * Contact: muhammedkalender@protonmail.com
 * Date: 16-03-2019
 * Time: 14:10
 */

include_once "include/functions.php";
include_once "include/db.php";
include_once "lang/tr.php"; //todo

$user = new User();

class InputMethod
{
    const GET = 0;
    const POST = 1;
    const REQUEST = 2; //todo
    const SESSION = 3;
}

class InputType
{
    const Int = 0;
    const Float = 1;
    const String = 2; //0-9, a-Z, . , _ - + - / *
    const Text = 3; //Replace all
    const URL = 4;
    const Email = 5;
    const Phone = 6; //todo
    const Bool = 7; //todo
    const Html = 8; //todo
    const Date = 9; //todo
}

class InputObject
{
    public $isCorrect = 1;
    public $name = "";
    public $langName = "";
    public $minLength = 0;
    public $maxLength = 0;
    public $method = 0;
    public $isNullable = false;
    public $inputType;

    /*
     * Dil kısmı boş bırakılırsa dil dosyasında [var.] şeklinde prefixle aratır
     */
    public function __construct($paramName, $paramLangName, int $paramMinLength, int $paramMaxLength, int $paramInputType, int $paramMethod = InputMethod::POST, bool $paramNullable = false)
    {
        if ($paramName == "" || $paramName == null) {
            $this->isCorrect = 0;
        }

        $this->name = $paramName;
        $this->langName = $paramLangName;
        $this->minLength = $paramMinLength;
        $this->maxLength = $paramMaxLength;
        $this->method = $paramMethod;
        $this->isNullable = $paramNullable;
        $this->inputType = $paramInputType;

        if ($this->langName == null || $this->langName == "") {
            $this->langName = l("var_" . $paramName);
        }
    }
}

class Log
{
    public static function Error($paramName, $paramMessage)
    {
        if (get_class($paramMessage) == Exception::class) {

        }
        //todo
    }
}

class Cache
{
    const CACHE_TIME = 43200; //12 Hours

    public function Valid(User $paramUser, $paramSuffix)
    {
        $fileURL = $this->Encrypt($paramUser->power . $paramUser->email . $paramSuffix);

        return $this->CheckFile($fileURL);
    }

    public function Encrypt($paramRaw)
    {
        //todo

        return sha1($paramRaw);
    }

    public function CheckFile($paramURL)
    {
        //todo

        $paramURL = "cache/" . $paramURL . ".html";

        if (file_exists($paramURL)) {
            if (time() < filemtime($paramURL) + Cache::CACHE_TIME) {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }
}

class DB
{
    public static function Execute($paramQuery)
    {
        global $db;
        return $db->prepare($paramQuery)->execute();
    }

    public static function ExecuteId($paramQuery)
    {
        global $db;
        return [$db->prepare($paramQuery)->execute(), $db->lastInsertId()];
    }

    public static function Select($paramQuery)
    {
        try {
            global $db;
            $conn = $db->prepare($paramQuery);

            if ($conn->execute() == false) {
                return array(false, $conn->errorInfo());
            }

            return [true, $conn->fetchAll(PDO::FETCH_ASSOC)];
        } catch (Exception $e) {
            return [false, $e];
        }
    }

    /*
     * Koşul sağlanırsa false, sağlanmazsa true döner
     */
    public static function Available($paramQuery)
    {
        return self::Select($paramQuery)[1] == null ? false : true;
    }
}

class Session
{

    public static function Delete($paramName)
    {
        //https://stackoverflow.com/a/18542272
        if (session_status() == PHP_SESSION_NONE) {
            return false;
        }

        unset($_SESSION[$paramName]);

        return true;
    }

    public static function Get($paramName)
    {
        //https://stackoverflow.com/a/18542272
        if (session_status() == PHP_SESSION_NONE) {
            return "";
        }

        return isset($_SESSION[$paramName]) ? $_SESSION[$paramName] : "";
    }

    public static function Set($paramName, $paramValue)
    {
        //https://stackoverflow.com/a/18542272
        if (session_status() == PHP_SESSION_NONE) {
            return false;
        }

        $_SESSION[$paramName] = $paramValue;

        return true;
    }
}

class Text
{
    public static function Token($paramLength = 128)
    {
        $list = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $listLength = strlen($list);

        $token = "";

        for ($i = 0; $i < $paramLength; $i++) {
            $randomIndex = rand(0, $listLength - 1); //todo -1
            $token .= $list[$randomIndex];
        }

        return $token;
    }

    public static function Encode($rawText)
    {
//todo
    }

    public static function Decode($rawText)
    {
//todo
    }

    //todo, değer doğru ise sonradan do-ğru formata döndür [ phone sorsun, doğru ise istenilen formatta biçsin)
    public static function Inputs($arrInputs)
    {
        foreach ($arrInputs as $input) {
            if (!$input->isCorrect) {
                return array(0, l("check_error", $input->langName));
            }

            if ($input->minLength == 0) {
                $input->isNullable = true;
            }

            $_checkMethod = false;
            $_var = "";

            if ($input->method == InputType::Int || $input->method == InputType::Float) {
                $_var = 0;
            }

            switch ($input->method) {
                case InputMethod::GET:
                    $_checkMethod = isset($_GET[$input->name]);

                    if ($_checkMethod) {
                        $_var = $_GET[$input->name];
                    }
                    break;
                case InputMethod::POST:
                    $_checkMethod = isset($_POST[$input->name]);

                    if ($_checkMethod) {
                        $_var = $_POST[$input->name];
                    }
                    break;
                case InputMethod::REQUEST:
                    $_checkMethod = isset($_REQUEST[$input->name]);

                    if ($_checkMethod) {
                        $_var = $_REQUEST[$input->name];
                    }
                    break;
                case InputMethod::SESSION:
                    $_checkMethod = Session::Get($input->name) == "" ? false : true;

                    if ($_checkMethod) {
                        $_var = Session::Get($input->name);
                    }
                    break;
                default:
                    $_checkMethod = 0;
            }

            if (!$_checkMethod && !$input->isNullable || (!$input->isNullable && ($_var == null && $_var === ""))) {
                return array(0, l("check_null", l($input->langName)));
            }

            if ($input->minLength > 0) {
                if (strlen($_var) < $input->minLength) {
                    return array(0, l("check_short", l($input->langName), $input->minLength));
                }
            }

            if ($input->maxLength > 0) {
                if (strlen($_var) > $input->maxLength) {
                    return array(0, l("check_long", l($input->langName), $input->maxLength));
                }
            }

            $_checkInputType = false;

            switch ($input->inputType) {
                case InputType::Int:
                    $_checkInputType = is_int($_var);
                    break;
                case InputType::Float:
                    $_checkInputType = is_float($_var);
                    break;
                case InputType::Email:
                    $_checkInputType = filter_var($_var, FILTER_VALIDATE_EMAIL);
                    break;
                case InputType::String:
                    //todo
                    $_checkInputType = true;
                    break;
                case InputType::Text:
                    //todo;
                    $_checkInputType = true;
                    break;
                case InputType::URL:
                    $_checkInputType = filter_var($_var, FILTER_VALIDATE_URL);
                    break;
                case InputType::Bool:
                    //todo
                    $_checkInputType = is_int($_var);

                    if ($_checkInputType) {
                        $_checkInputType = $_var == 0 || $_var == 1;
                    }
                    break;
                case InputType::Html:
                    //todo
                    $_checkInputType = true;
                    if ($_var == 0) {
                        $_var = "";
                    }
                    break;
                case InputType::Phone:
                    //todo
                    $_checkInputType = true;
                    if ($_var == 0) {
                        $_var = "";
                    }
                    break;
                case InputType::Date:
                    //todo
                    $_checkInputType = true;
                    if ($_var == 0) {
                        $_var = "";
                    }
                    break;
                default:
                    $_checkInputType = false;
            }

            if (!$_checkInputType) {
                return array(0, l("check_type", $input->langName));
            }

            switch ($input->method) {
                case InputMethod::GET:
                    $_GET[$input->name] = $_var;
                    break;
                case InputMethod::POST:
                    $_POST[$input->name] = $_var;
                    break;
                case InputMethod::REQUEST:
                    $_REQUEST[$input->name] = $_var;
                    break;
                case InputMethod::SESSION:
                    Session::Set($input->name, $_var);
                    break;
            }
        }

        return array(1);
    }
}

//todo
class Auth
{
    const LEVEL_VISITOR = 1;
    const LEVEL_MEMBER = 2;
    const LEVEL_COMPANY = 3;
    const LEVEL_MOD = 4;
    const LEVEL_ADMIN = 5;

    const TYPE_UPPER_VISITOR = 0;
    const TYPE_ONLY_HIMSELF = 1;
    const TYPE_HIMSELF_UPPER_MEMBER = 2;
    const TYPE_HIMSELF_UPPER_MOD = 3;
    const TYPE_HIMSELF_UPPER_COMPANY = 4;
    const TYPE_HIMSELF_UPPER_ADMIN = 5;

    const TYPE_UPPER_MEMBER = 6;
    const TYPE_UPPER_MOD = 7;
    const TYPE_UPPER_COMPANY = 8;
    const TYPE_UPPER_ADMIN = 9;

    //İşlemi yapmak için, 2. parametreden yüksek olması gerekiyor
    const TYPE_UPPER_THIS = 10;
    const TYPE_UPPER = 11;

    const INSERT_USER = Auth::LEVEL_MOD;

    const INSERT_COMPANY = [Auth::LEVEL_COMPANY, Auth::TYPE_UPPER];
    const UPDATE_COMPANY = [Auth::LEVEL_COMPANY, Auth::TYPE_UPPER];
    const DELETE_COMPANY = [Auth::LEVEL_COMPANY, Auth::TYPE_UPPER];
}

class User
{
    const MILITARY_NS = 0;
    const MILITARY_PENDING = 1; //Tecilli
    const MILITARY_EXEMPT = 2; //Muaf
    const MILITARY_OK = 3; //Bitti

    const DISABLE_NONE = 0;
    const DISABLE_X = 1;

    const SMOKE_NS = 0;
    const SMOKE_NO = 1;
    const SMOKE_YES = 2;

    const GENDER_NS = 0;
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    const USER = 0;
    const COMPANY = 1;

    public $id, $username, $email, $power, $register_date, $name;
    public $ip, $token, $token_key, $type = User::USER;
    public $gsm;

    //Şirket İçin
    public $description, $website, $picture, $tel, $fax;

    //Kullanıcı İçin  [TODO MILITARY DATE DB ]
    public $surname, $disable = USER::DISABLE_NONE, $smoke = User::SMOKE_NS;
    public $military = User::MILITARY_NS, $military_date;

    //Register kısmını ilgilendiriyor
    public $password, $password_prefix, $gender = User::GENDER_NS;

    public function __construct()
    {
        $this->Get();
    }

    public function CheckData()
    {
        $check = Text::Inputs([
            new InputObject("username", "", 0, 32, InputType::String),
            new InputObject("email", "", 6, 48, InputType::Email),
            new InputObject("type", "", 1, 2, InputType::Int),
            new InputObject("name", "", 3, 32, InputType::String),
            new InputObject("description", "", 0, 4096, InputType::Html),
            new InputObject("website", "", 0, 1024, InputType::URL),
            new InputObject("picture", "", 0, 1024, InputType::URL),
            new InputObject("password", "", 6, 32, InputType::Text),
            new InputObject("password_repeat", "", 6, 32, InputType::Text),
        ]);

        if ($check[0] == false) {
            return $check;
        }

        if ($_POST["password"] != $_POST["password_repeat"]) {
            return [false, l("password_repeat")];
        }

        if ($_POST["type"] == User::USER) {
            $check = Text::Inputs([
                new InputObject("gsm", "", 0, 48, InputType::Phone),
                new InputObject("surname", "", 0, 32, InputType::String),
                new InputObject("disable", "", 0, 2, InputType::Int),
                new InputObject("smoke", "", 0, 1, InputType::Bool),
                new InputObject("military", "", 0, 2, InputType::Bool),
                new InputObject("military_date", "", 0, 32, InputType::Date),
                new InputObject("gender", "", 0, 2, InputType::Int)
            ]);

            if ($check[0] == false) {
                return $check;
            }

            $this->surname = $_POST["surname"];
            $this->gsm = $_POST["gsm"];

            //TODO seçeneklerden birimi diye bak
            $this->disable = $_POST["disable"];
            $this->smoke = $_POST["smoke"];
            $this->military = $_POST["military"];
            $this->military_date = $_POST["military_date"];
            $this->gender = $_POST["gender"];
        } else if ($_POST["type"] == User::COMPANY) {
            $check = Text::Inputs([
                new InputObject("tel", "", 0, 32, InputType::Phone),
                new InputObject("fax", "", 0, 32, InputType::Phone)
            ]);

            if ($check[0] == false) {
                return $check;
            }

            $this->tel = $_POST["tel"];
            $this->fax = $_POST["fax"];
        } else {
            return [false, l("hack")];
        }

        $this->username = $_POST["username"];
        $this->email = $_POST["email"];
        $this->type = $_POST["type"]; //todo seçeneklerden birimi
        $this->name = $_POST["name"];
        $this->description = $_POST["description"];
        $this->website = $_POST["website"];
        $this->picture = $_POST["picture"];

        $this->password_prefix = Text::Token(64);
        $this->password = User::Password($_POST["password"], $this->password_prefix);

        if (DB::Available("SELECT user_id FROM user WHERE user_email = '{$_POST["email"]}'")) {
            return [false, l("already_email")];
        }

        if (DB::Available("SELECT user_id FROM user WHERE user_nick = '{$_POST["username"]}'")) {
            return [false, l("already_username")];
        }

        return [true];
    }

    public function Auth($pWork, $pMode, $pCompare = -1)
    {
        //todo, değişecek Gereken, LOW, UP, EQ, This, Compore
        return true;
    }

    public function Register(User $pUser = null)
    {
        if ($pUser == null) {
            $pUser = $this;
        } else {
            //todo auth
            if ($this->type == User::COMPANY) {
                if ($this->Auth(Auth::INSERT_COMPANY, Auth::TYPE_UPPER_MOD)) {
                    return [false, l("auth_problem")];
                }
            }

            if ($this->type == User::COMPANY) {
                if ($this->Auth(Auth::INSERT_USER, Auth::TYPE_UPPER_MOD)) {
                    return [false, l("auth_problem")];
                }
            }
        }

        if (($result = $pUser->CheckData())[0] == false) {
            return $result;
        }

        //      global $db;

        $qInsert = "";
//todo, askerli kbitiş db
        if ($pUser->type == User::USER) {
            $qInsert = "INSERT INTO user (user_type, user_nick, user_email, user_password, user_prefix, user_name, user_surname, user_gsm, user_description,  user_gender, user_military, user_special, user_smoke, user_website, user_picture) VALUES (
  {$this->type}, '{$this->username}','{$this->email}','{$this->password}','{$this->password_prefix}','{$this->name}','{$this->surname}','{$this->gsm}','{$this->description}','{$this->gender}','{$this->military}','{$this->disable}','{$this->smoke}','{$this->website}','{$this->picture}')";
        }

        if ($pUser->type == User::COMPANY) {
            $qInsert = "INSERT INTO user (user_type, user_nick, user_email, user_password, user_prefix, user_name, user_tel, user_gsm, user_fax, user_description, user_website, user_picture) VALUES (
  {$this->type}, '{$this->username}','{$this->email}','{$this->password}','{$this->password_prefix}','{$this->name}','{$this->tel}','{$this->gsm}','{$this->fax}','{$this->description}','{$this->website}','{$this->picture}')";
        }

        return DB::ExecuteId($qInsert);
    }

    //todo algoritma
    public static function Password($pRawPassword, $pPrefix)
    {
        return sha1(md5($pRawPassword . md5($pPrefix)));
    }

    public function Login()
    {
        $check = Text::Inputs([
            new InputObject("user", "login_user", 3, 128, InputType::String),
            new InputObject("password", "login_password", 6, 64, InputType::String)
        ]);

        if ($check[0] == false) {
            return $check;
        }

        $prefix = DB::Select("SELECT user_prefix FROM user WHERE (user_email = '{$_POST["email"]}' OR user_nick = '{$_POST["user"]}')");

        if ($prefix[0] == false) {
            return [false, l("wrong_login")];
        }

        $password = User::Password($_POST["password"], $prefix[1][0]["user_prefix"]);
        $checkLogin = DB::Select("SELECT user_id FROM user WHERE (user_email = '{$_POST["email"]}' OR user_nick = '{$_POST["user"]}') AND user_password = '{$password}'");

        if (isset($checkLogin[1][0]["user_id"]) == false) {
            return [false, l("wrong_login")];
        }

        $user_id = $checkLogin[1][0]["user_id"];

        if (is_int($user_id) == false || intval($user_id) < 1) {
            return [false, l("wrong_login")];
        }

        $token = Text::Token();
        $token_key = Text::Token();

        while (DB::Available("SELECT token_id FROM token WHERE user_id = {$user_id} AND token =  '{$token}' AND token_key = '{$token_key}' AND active = 1")) {
            $token = Text::Token();
            $token_key = Text::Token();
        }

        DB::Execute("UPDATE token SET active = 0 WHERE user_id = {$user_id}");

        $qInsert = DB::ExecuteId("INSERT INTO token (user_id, token, token_key) VALUES ({$user_id}, '{$token}', '{$token_key}')");

        if ($qInsert[0]) {
            //todo redirect
            Session::Set("token_id", $qInsert);
            Session::Set("token", $token);
            Session::Set("token_key", $token_key);
            Session::Set("user_id", $user_id);

            return [true, $qInsert[1], $token, $token_key];
        } else {
            return [false, l("login_system_error")];
        }

    }

    public function Get(int $pUserId = 0)
    {
        if ($pUserId != 0) {
            //todo auth Dışarıdan kullanıcının bilgisi getirilmek isteniyor
        }

        $userId = Session::Get("user_id");
        $token = Session::Get("token");
        $token_key = Session::Get("token_key");

        if ($userId == "" || $token == "" || $token_key == "") {
            //todo, visitor
            return false;
        }

        $qToken = DB::Select("SELECT token_id FROM token WHERE token = '{$token}' AND token_key = '{$token_key}' AND user_id = '{$userId}' AND active = 1 ");

        $pUserId = $userId;

        if ($qToken[0] == false || $qToken[1] == null) {
            return false;
        }

        $qUser = DB::Select("SELECT * FROM user WHERE user_id = {$pUserId}");

        if ($qUser[0] == false || isset($qUser[1][0]) == false) {
            return false;
        }

        $qUser = $qUser[1][0];

        $this->id = $qUser["user_id"];
        $this->username = $qUser["user_nick"];
        $this->type = $qUser["user_type"];
        $this->power = $qUser["user_power"];
        $this->email = $qUser["user_email"];

        $this->name = $qUser["user_name"];
        $this->surname = $qUser["user_surname"];

        $this->gsm = $qUser["user_gsm"];
        $this->tel = $qUser["user_tel"];
        $this->fax = $qUser["user_fax"];

        $this->disable = $qUser["user_special"];

        $this->smoke = $qUser["user_smoke"];
        $this->military = $qUser["user_military"];

        //todo $this->military_date = $qUser["user_"];

        $this->gender = $qUser["user_gender"];

        $this->token = $token;
        $this->token_key = $token_key;

        return true;
    }

    function GetIP()
    {
        try {
            $ip = null;

            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }

            return $ip;
        } catch (Exception $e) {
            Log::Error("ip", $e);
            return "ERR";
        }
    }
}


