-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 18, 2019 at 12:38 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kariyer`
--

-- --------------------------------------------------------

--
-- Table structure for table `certificate`
--
-- Error reading structure for table kariyer.certificate: #1932 - Table 'kariyer.certificate' doesn't exist in engine
-- Error reading data for table kariyer.certificate: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `kariyer`.`certificate`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `experience`
--

CREATE TABLE `experience` (
  `experience_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `experience_name` int(11) NOT NULL,
  `experience_position` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `experience_start_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `experience_end_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `experience_description` varchar(4096) COLLATE utf8_unicode_ci NOT NULL,
  `experience_insert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `experience_row` int(11) NOT NULL DEFAULT '0',
  `experience_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `referance`
--

CREATE TABLE `referance` (
  `referance_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `referance_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `referance_position` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `referance_phone` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `referance_mail` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `referance_description` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `referance_insert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `referance_row` int(11) NOT NULL DEFAULT '0',
  `referance_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `school`
--

CREATE TABLE `school` (
  `school_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `school_name` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `school_department` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `school_description` varchar(4096) COLLATE utf8_unicode_ci NOT NULL,
  `school_start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `school_end_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `school_department_type` int(11) NOT NULL DEFAULT '0',
  `school_insert` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `school_row` int(11) NOT NULL DEFAULT '0',
  `school_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `token_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `token_key` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `token_inser` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `token`
--

INSERT INTO `token` (`token_id`, `user_id`, `token`, `token_key`, `token_inser`, `active`) VALUES
(1, 1, 'BBdclLXRIuXUPcmBn9NAz2IILSdqWLF08kbNEk1cZfIqgm1sXmtOFqy8YCf6vLNpxwl5QB8pHN5456yXCC3OXuhYeGEyGCqAGWXAiEPTCYR8d9HkKPke5LbZ57z77xE4', '2u0FXmyvehIB0IyJkEdtuA9sC73bAssb6FIeQHYtGRQ1O0zKLY3UEeitz7g1Os9AlGFaXhcGn6nfejE2QJ6tTKAoaWD8De88HszNcXGPug1YaFOXV0bsPoDdSQ5yuHOu', '2019-03-17 23:33:31', 0),
(2, 1, '6HtI0yAXbNlWwun3OHm3NH9dFatGGtHVcRmF010FIugy08fLa72KLM0ZsK20HyX3TrOHXVafcuX6sE4KI9Ch3oc6Q9HwpfrqkUnxb8iYIOMlsWlVi0uaSvHSHIMC9JgD', 'b289skCckOipBPK1Xixzb1ivAI06FbnZ9felC1OMles2YRX7QpIR0yxk8hgmKXx0RHPBG409xJX9upLRrngLUdrA641bjWop5JKmLd4oOYbXSLU937Jek9A6R15dvXAI', '2019-03-17 23:34:46', 0),
(3, 1, 'QmdJhUzLBoMBxnlPo0aqtbCLypBHBelgUPXL0i9yEtd8KzLW9zz5szKvg2KnF74xhLF0AuUXY0QXgCbfRiHy6bHUscBfYkEul8RMyImj9ogiOMQmUUYP7DiqEavi3A1O', 'TSWGKrWxmoc8v0snVGVvHAPehbzT5q69yAdy5I8wzhDX6Yx2VJPOqWgMSaLMBCU29qpDcHqXWIpR3RTvXGbOYPyweBHh9yfDIKgqlMkK8VSBW5l6LzoGYAJ3Mgk4VQSI', '2019-03-17 23:36:08', 0),
(4, 1, 'ZcEOJeB6IyROD9wsonweBy9jVrzrlCO6ko4vTl7Q3aY4PAuZ1ABRtjV0VHRkdUhsRVB1PItpWmeif0o0AfrbX4vIvbQmyScW4rURsTFCoVy8RrEWNewAiprAaAvw66yc', 'FBtzwCqO1ZjkE57jBzxV9rumI30sJaX5k9TFhff7km8bB22n6r3TNpsoINBS19OzfR3OSDDKOxhmGRi6UE3l4fJvAcehzib0jQcQC8y1gZRVVavcOC0YaHEG86DQB2PR', '2019-03-17 23:36:31', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `user_email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `user_password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `user_prefix` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `user_nick` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `user_surname` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_description` varchar(8096) COLLATE utf8_unicode_ci NOT NULL,
  `user_gsm` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `user_tel` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `user_fax` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` int(11) NOT NULL DEFAULT '1',
  `user_email_confirm` tinyint(1) NOT NULL DEFAULT '0',
  `user_tel_confirm` tinyint(1) NOT NULL DEFAULT '0',
  `user_banend` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_active` tinyint(1) NOT NULL DEFAULT '1',
  `user_gender` int(11) NOT NULL DEFAULT '0',
  `user_military` int(11) NOT NULL DEFAULT '0',
  `user_special` int(11) NOT NULL DEFAULT '0',
  `user_smoke` tinyint(1) NOT NULL DEFAULT '0',
  `user_website` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `user_picture` varchar(512) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `user_email`, `user_password`, `user_prefix`, `user_nick`, `user_surname`, `user_description`, `user_gsm`, `user_tel`, `user_fax`, `user_type`, `user_email_confirm`, `user_tel_confirm`, `user_banend`, `user_active`, `user_gender`, `user_military`, `user_special`, `user_smoke`, `user_website`, `user_picture`) VALUES
(1, 'Hasan', 'a@g.com', 'cd3aad69a22e945e26b87a1b33a1e03bde2b2023', 'JDig47aEql13dIWCFbanKnjAqWRsK2vqeWOhVg3Aybcw7uasnUSR04GxRqMRvgfU', 'Test', 'Karabasan', '', '', '', '', 0, 0, 0, '0000-00-00 00:00:00', 1, 0, 0, 0, 0, 'https://web.sitesi.com', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `referance`
--
ALTER TABLE `referance`
  ADD PRIMARY KEY (`referance_id`);

--
-- Indexes for table `school`
--
ALTER TABLE `school`
  ADD PRIMARY KEY (`school_id`);

--
-- Indexes for table `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`token_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `referance`
--
ALTER TABLE `referance`
  MODIFY `referance_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `school`
--
ALTER TABLE `school`
  MODIFY `school_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `token`
--
ALTER TABLE `token`
  MODIFY `token_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
